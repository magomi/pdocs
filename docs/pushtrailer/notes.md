* Einspur-Anhänger
    * kann auch auf rauherem Untergrund gefahren werden (unbefestiger Radweg,...)
    * stabile Befestigung rechts und links am Hinterad
    * verfügbare Transportfläche vermutlich sub-optimal, muss getestet werden.
    * Rad ist hinten beidseitig eingespannt -> normaler Nabenmotor kann zum Einsatz kommen 

* Antrieb
    * Anhänger ggf. nur 16"-Rad
        * Durchmesser: 40,96 cm
        * Umfang: 128,68 cm
        * 255 RPM -> max. 7,43 km/h -> viel zu langsam
        * min 800 RPM notwendig
        * alternativ Umbau 20"-Rad -> 18,22 km/h bei 500 RPM

* Controller
    * Standard-Controller KT-xxx
    * ggf. längere Leitungen und Kupplung zur Bedienung am Fahrrad
    * freie Firmware, insbesondere für Experimente/PoC notwendig

* 2022-07-20
    * M-Wave Single 40 geliefert
        * für 20"-Räder auf jeden Fall zu klein, perspektivisch ggf. hinten Radaufnahme verlängern
