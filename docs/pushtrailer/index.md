# Schiebe-Anhänger fürs Fahrrad

## Warum?

Zwischen E-Bike und Bio-Bike klafft noch eine sehr große Lücke.

Für mich sieht die folgendermaßen aus: für normale Fahrten reicht mein Trecking-Rad vollkommen aus. Im Gegenteil - aus diversen Gründen will ich gar kein E-Bike nutzen. Will ich aber das Rad z. B. für Einkaufsfahrten benutzen dan komme ich hier (Hügelland, die letzten 1,5 km sind immer mit ca. 10 % Steigung) nicht mehr weiter. Den 20-Kg-Wocheneinkauf bekomme ich ohne Unterstützung nicht nach hause.

Nur für den o.g. Zweck ein E-Bike anschaffen ist weder ökonomisch noch ökologisch nachhaltig. Immer weiter mit dem Auto zu fahren kommt irgendwie auch nicht in Frage. 

Ein angetriebener (Lasten-)Anhänger am Bio-Bike erscheint mir die passende Lösung für das Problem.

## Content

* [Links](links.md)
* [Notizen](notes.md)