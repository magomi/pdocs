# Discalaimer

Ich sammle hier für diverse private Projekte die für mich relvanten Informationen: Links, Ablaufbeschreibungen, Zeichnungen, Pläne,...

Falls irgendetwas sinnvolles dabei sein sollte dann kann das gerne weiter genutzt werden. **Bedenkt aber, dass ich keinerlei Haftung übernehme. Insbesondere bei Dingen mit Strom gilt: lasst die Finger davon solange ihr nicht eine abgeschlossene einschlägige Berufsausbildung habt!**

Ich freue mich über jegliches Feedback, schreibt gerne an [projekte@grunert.dev](mailto://projekte@grunert.dev)
